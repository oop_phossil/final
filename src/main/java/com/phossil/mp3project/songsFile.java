/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.phossil.mp3project;

import java.io.File;

/**
 *
 * @author HotPC
 */
public class songsFile {
    private String name;
    private File file;
    
    public songsFile(String name, File file) {
        this.name = name;
        this.file = file;
    }
    
    public String getName() {
        return name;
    }
    
    public File getFile() {
        return file;
    }
    
    @Override
    public String toString() {
        return "Song ( "+name+" )";
    }
    
}
